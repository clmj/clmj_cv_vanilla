var slideIndex = 1;
var bubbleIndex = 1;

//Changement de Bubble
function boiTalk(nb) {
    //////BOI
    //RESET
    document.getElementById("boi"+bubbleIndex).classList.remove("boiOn");
    var resetAnimBoi = document.getElementsByClassName("mouth");
    for (i = 0; i < resetAnimBoi.length; i++) {
        resetAnimBoi.item(i).classList.remove("mouthAnim");
    }
    //AFFICHAGE
    document.getElementById("boi"+nb).classList.add("boiOn");
    var animBoi = document.getElementsByClassName("mouth");
    switch (nb) {
        case 1 :
        animBoi.item(0).classList.add("mouthAnim");
        animBoi.item(1).classList.add("mouthAnim");
        break;
        case 2 :
        animBoi.item(2).classList.add("mouthAnim");
        animBoi.item(3).classList.add("mouthAnim");
        break;
        case 3 :
        animBoi.item(4).classList.add("mouthAnim");
        animBoi.item(5).classList.add("mouthAnim");
        break;
    }

    //////BUBBLE
    //RESET
    document.getElementById("bubble").classList.remove("bubble"+bubbleIndex);
    //AFFICHAGE
    document.getElementById("bubble").classList.add("bubble"+nb);
    
    //////BUTTON
    //RESET
    document.getElementById("btn"+bubbleIndex).classList.remove("btn"+bubbleIndex+"On");
    //AFFICHAGE
    document.getElementById("btn"+nb).classList.add("btn"+nb+"On");

    //////TICK
    //RESET
    document.getElementById("tick").classList.remove("tick"+bubbleIndex);
    //AFFICHAGE
    document.getElementById("tick").classList.add("tick"+nb);
    
    //////SLIDES
    //RESET
    document.getElementById("bubble"+bubbleIndex+"-slide"+slideIndex).style.display = "none";
    //MAJ INDEXES
    bubbleIndex = nb;
    slideIndex = 1;
    //AFFICHAGE
    slide(0);
}

//Changement de slide
function slide(direction) {
    //reset slide
    document.getElementById("bubble"+bubbleIndex+"-slide"+slideIndex).style.display="none";
    //maj slideIndex
    slideIndex += direction;
    //affiche slide
    document.getElementById("bubble"+bubbleIndex+"-slide"+slideIndex).style.display="inline";
    //reset arrows
    var arrows = document.getElementsByClassName("arrow");
    for (i = 0; i < arrows.length; i++) {
            arrows.item(i).style.visibility="hidden";
        }
    //affiche arrows
    if (bubbleIndex == 1 || bubbleIndex == 2) {
        switch (slideIndex) {
            case 1 :
            document.getElementById("arrowRight").style.visibility = "visible";
            break;
            case 2 :
            document.getElementById("arrowLeft").style.visibility = "visible";
            document.getElementById("arrowRight").style.visibility = "visible";
            break;
            case 3 :
            document.getElementById("arrowLeft").style.visibility = "visible";
            break;
        }
    }
}

//Close Sent Pop Up
function closeSent() {
    document.getElementById("sent").style.display = "none";
}